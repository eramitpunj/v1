import React, { Component } from "react";
import { Switch, Redirect, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Header from "./pages/Header";
import LLayoutRoute from "./pages/layouts/LLayoutRoute";
import PlainLayoutRoute from "./pages/layouts/PlainLayoutRoute";
import Login from "./features/admin/Login";
import Menu from "./features/admin/Menu";
import Dashboard from "./features/admin/Dashboard";
import RLayoutRoute from "./pages/layouts/RLayoutRoute";
import CreateDepartment from "./features/admin/CreateDepartment";
import CreateItem from "./features/admin/CreateItem";
import ItemDetails from "./features/Item/ItemDetails";
import DepartmentList from "./features/Item/DepartmentList";
import ItemList from "./features/Item/ItemList";
import Cart from "./features/cart/Cart";
import Checkout from "./features/checkout/Checkout";

class App extends Component {
  render() {
    return (
      <div className="container">
        <Header {...this.props} />
        <div className="row p-3">
          <Switch>
            <Route exact path="/" render={() => <Redirect to="items/1" />} />
            <LLayoutRoute
              exact
              path="/items/:page"
              leftComponent={DepartmentList}
              rightComponent={ItemList}
            />
            <LLayoutRoute
              exact
              path="/admin"
              leftComponent={Menu}
              rightComponent={Dashboard}
            />
            <PlainLayoutRoute exact path="/login" component={Login} />
            <LLayoutRoute
              exact
              path="/admin/createItem"
              leftComponent={Menu}
              rightComponent={CreateItem}
            />
            <LLayoutRoute
              exact
              path="/admin/createDepartment"
              leftComponent={Menu}
              rightComponent={CreateDepartment}
            />
            <RLayoutRoute
              exact
              path="/cart"
              leftComponent={Cart}
              rightComponent={Checkout}
            />

            <PlainLayoutRoute
              exact
              path="/item/:itemId"
              component={ItemDetails}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart
  };
}

export default withRouter(connect(mapStateToProps)(App));
