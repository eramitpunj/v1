import React from "react";
import { Fragment } from "react";
import { Route } from "react-router-dom";

const LLayoutRoute = ({
  leftComponent: LeftComponent,
  rightComponent: RightComponent,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <Fragment>
          <div className="col-3 alert alert-secondary">
            <LeftComponent {...matchProps} />
          </div>
          <div className="col">
            <RightComponent {...matchProps} />
          </div>
        </Fragment>
      )}
    />
  );
};

export default LLayoutRoute;
