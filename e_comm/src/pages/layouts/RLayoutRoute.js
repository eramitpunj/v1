import React from "react";
import { Fragment } from "react";
import { Route } from "react-router-dom";

const RLayoutRoute = ({
  leftComponent: LeftComponent,
  rightComponent: RightComponent,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <Fragment>
          <div className="col">
            <LeftComponent {...matchProps} />
          </div>
          <div className="col-3 alert alert-secondary">
            <RightComponent {...matchProps} />
          </div>
        </Fragment>
      )}
    />
  );
};

export default RLayoutRoute;
