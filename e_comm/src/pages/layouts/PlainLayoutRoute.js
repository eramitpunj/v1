import React from "react";
import { Route } from "react-router-dom";

const PlainLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <div className="col">
          <Component {...matchProps} />
        </div>
      )}
    />
  );
};

export default PlainLayoutRoute;
