import React, { Component } from "react";
import { withRouter } from "react-router";
import { NavLink } from "react-router-dom";
import { AUTH_TOKEN } from "./../common/constants";
const clickableDiv = {
  cursor: "pointer"
};

export class Header extends Component {
  render() {
    const { cart } = this.props;
    const authToken = localStorage.getItem(AUTH_TOKEN);
    return (
      <div className="my-3 alert alert-info">
        <h4>E-Commerce</h4>
        <div className="row justify-content-between">
          <div className="col-5">
            <NavLink to="/" className="alert-link">
              Home
            </NavLink>
            {" | "}
            <NavLink to="/cart" className="alert-link">
              Cart (
              {cart.reduce((acc, saleItem) => {
                return acc + saleItem.qty;
              }, 0)}
              )
            </NavLink>
            {" | "}
            {authToken && (
              <NavLink to="/admin" className="alert-link">
                Admin
              </NavLink>
            )}
            {authToken && " | "}
          </div>
          <div className="col-5 text-right">
            {authToken ? (
              <div
                className="alert-link pointer"
                style={clickableDiv}
                onClick={() => {
                  localStorage.removeItem(AUTH_TOKEN);
                  this.props.history.push("/");
                }}
              >
                Logout
              </div>
            ) : (
              <NavLink to="/login" className="alert-link">
                Login
              </NavLink>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Header);
