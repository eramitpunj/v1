import { combineReducers } from "redux";
import itemReducer from "./Item/ItemReducer";
import cartReducer from "./cart/CartReducer";

const rootReducer = combineReducers({
  items: itemReducer,
  cart: cartReducer
});

export default rootReducer;
