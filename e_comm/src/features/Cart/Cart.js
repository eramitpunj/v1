import React, { Component } from "react";
import { connect } from "react-redux";

export class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: 1
    };
  }
  render() {
    const props = this.props;
    return (
      <div>
        {props.cart.length <= 0 ? (
          <div className="alert alert-danger">
            There are no items in the Cart!!
          </div>
        ) : (
          props.cart.map((saleItem, index) => (
            <div className="row mb-3" key={index}>
              <div className="col-3">
                <img
                  className="img-fluid"
                  src={saleItem.url}
                  alt={saleItem.name}
                />
              </div>
              <div className="col-6 text-left">
                <h5>
                  {saleItem.name}{" "}
                  <span className="text-danger">
                    {"     $ "}({saleItem.price.toFixed(2)})
                  </span>
                </h5>
                <br />
                <p>{saleItem.description}</p>
              </div>
              <div className="col text-left">
                <div className="input-group">
                  <input
                    type="number"
                    step="1"
                    className="form-control"
                    value={saleItem.qty}
                    onChange={e => this.setState({ qty: saleItem.qty })}
                  />
                  <div className="input-group-append">
                    <button
                      className="btn btn-sm btn-outline-info"
                      onClick={e => props.addToCart(saleItem)}
                    >
                      +
                    </button>
                    <button
                      className="btn btn-sm btn-outline-danger"
                      onClick={e => props.removeFromCart(saleItem)}
                    >
                      -
                    </button>
                    <button
                      className="btn btn-sm btn-outline-danger"
                      onClick={e => props.removeAllFromCart(saleItem)}
                    >
                      X
                    </button>
                  </div>
                </div>

                <br />
                <h6 className="text-danger">
                  <b>
                    {" $ "}
                    {saleItem.total.toFixed(2)}
                  </b>
                </h6>
              </div>
            </div>
          ))
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addToCart: saleItem => {
      dispatch({ type: "ADD", payload: saleItem });
    },
    removeFromCart: saleItem => {
      dispatch({ type: "REMOVE", payload: saleItem });
    },
    removeAllFromCart: saleItem => {
      dispatch({ type: "REMOVE_ALL", payload: saleItem });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
