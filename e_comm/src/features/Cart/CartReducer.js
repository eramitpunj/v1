const cartWithoutItem = (cart, item) =>
  cart.filter(cartItem => cartItem.id !== item.id);

const itemInCart = (cart, item) =>
  cart.filter(cartItem => cartItem.id === item.id)[0];

const addToCart = (cart, item) => {
  const cartItem = itemInCart(cart, item);
  return cartItem === undefined
    ? [
        ...cartWithoutItem(cart, item),
        { ...item, qty: 1, tax: 0.0, total: item.price }
      ]
    : [
        ...cartWithoutItem(cart, item),
        {
          ...cartItem,
          qty: cartItem.qty + 1,
          tax: 0.0,
          total: (cartItem.qty + 1) * item.price
        }
      ];
};

const removeFromCart = (cart, item) => {
  return item.qty === 1
    ? [...cartWithoutItem(cart, item)]
    : [
        ...cartWithoutItem(cart, item),
        {
          ...item,
          qty: item.qty - 1,
          tax: 0.0,
          total: (item.qty - 1) * item.price
        }
      ];
};

const removeAllFromCart = (cart, item) => {
  return [...cartWithoutItem(cart, item)];
};

const cartReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD":
      return addToCart(state, action.payload);

    case "REMOVE":
      return removeFromCart(state, action.payload);

    case "REMOVE_ALL":
      return removeAllFromCart(state, action.payload);

    default:
      return state;
  }
};
export default cartReducer;
