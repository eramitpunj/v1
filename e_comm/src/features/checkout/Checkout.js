import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

export class Checkout extends Component {
  render() {
    const props = this.props;
    let totalTaxAmt = 0;
    let totalAmt = 0;
    totalAmt = parseFloat(
      props.cart.reduce((total, saleItem) => total + saleItem.total, 0)
    );
    totalAmt = isNaN(totalAmt) ? 0 : totalAmt;

    return (
      <div>
        {props.cart.length > 0 && (
          <Fragment>
            <div className="row">
              <div className="col-6">
                <h6>Total Tax</h6>
              </div>
              <div className="col">
                {" $ "}
                {totalTaxAmt.toFixed(2)}
              </div>
            </div>
            <div className="row my-2">
              <div className="col-6">
                <h6>Total Amount</h6>
              </div>
              <div className="col">
                {" $ "}
                {totalAmt.toFixed(2)}
              </div>
            </div>
            <div className="row my-4">
              <div className="col text-center">
                <button
                  type="button"
                  className="btn btn-success btn-sm"
                  onClick={() => this._checkOutCart(props.cart)}
                >
                  Complete Order
                </button>
              </div>
            </div>
          </Fragment>
        )}
      </div>
    );
  }

  _checkOutCart(cart) {
    alert("Thanks for your Order!!");
    // Fix this later on to actually order submit and resetting the cart
    window.location.href = "/";
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart
  };
}

export default withRouter(connect(mapStateToProps)(Checkout));
