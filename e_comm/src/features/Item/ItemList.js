import React, { Component, Fragment } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { connect } from "react-redux";
import { ITEMS_PER_PAGE } from "../../common/constants";
import Item from "./Item";

const ITEMS_QUERY = gql`
  query ItemsQuery($first: Int, $skip: Int, $orderBy: ItemOrderByInput) {
    items(first: $first, skip: $skip, orderBy: $orderBy) {
      count
      items {
        id
        name
        description
        url
        price
        createdBy {
          id
          name
        }
      }
    }
  }
`;

const NEW_ITEMS_SUBSCRIPTION = gql`
  subscription {
    newItem {
      id
      name
      description
      url
      price
      createdBy {
        id
        name
        email
      }
    }
  }
`;

export class ItemList extends Component {
  render() {
    const { addToCart, removeFromCart, cart } = this.props;
    return (
      <Query query={ITEMS_QUERY} variables={this._getQueryVariables()}>
        {({ loading, error, data, subscribeToMore }) => {
          if (loading)
            return (
              <div className="alert alert-warning">Fetching Item List...</div>
            );
          if (error)
            return (
              <div className="alert alert-danger">
                Error Retrieving Item List!!
              </div>
            );

          this._subscribeToNewItems(subscribeToMore);
          const items = data.items.items;
          const isNewPage = this.props.location.pathname.includes("items");

          return items.length > 0 ? (
            <Fragment>
              {isNewPage && (
                <div className="row mb-2">
                  <div className="col text-right">
                    <button
                      type="button"
                      className="btn btn-sm btn-secondary"
                      onClick={this._previousPage}
                    >
                      Previous
                    </button>
                    {"   "}
                    <button
                      type="button"
                      className="btn btn-sm btn-secondary"
                      onClick={() => this._nextPage(data)}
                    >
                      Next
                    </button>
                  </div>
                </div>
              )}

              {/* This section is to render the items*/}
              <div className="row">
                {items.map(item => (
                  <Item
                    item={item}
                    key={item.id}
                    addToCart={addToCart}
                    removeFromCart={removeFromCart}
                    cartItem={
                      cart.filter(cartItem => cartItem.id === item.id)[0]
                    }
                  />
                ))}
              </div>

              {isNewPage && (
                <div className="row mb-2">
                  <div className="col text-right">
                    <button
                      type="button"
                      className="btn btn-sm btn-secondary"
                      onClick={this._previousPage}
                    >
                      Previous
                    </button>
                    {"   "}
                    <button
                      type="button"
                      className="btn btn-sm btn-secondary"
                      onClick={() => this._nextPage(data)}
                    >
                      Next
                    </button>
                  </div>
                </div>
              )}
            </Fragment>
          ) : (
            <div className="alert alert-danger">No items found!!</div>
          );
        }}
      </Query>
    );
  }

  _getQueryVariables = () => {
    const isNewPage = this.props.location.pathname.includes("items");
    const page = parseInt(this.props.match.params.page, 10);

    const skip = isNewPage ? (page - 1) * ITEMS_PER_PAGE : 0;
    const first = isNewPage ? ITEMS_PER_PAGE : 100;
    const orderBy = isNewPage ? "createdOn_DESC" : null;
    return { first, skip, orderBy };
  };

  _subscribeToNewItems = subscribeToMore => {
    subscribeToMore({
      document: NEW_ITEMS_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newItem = subscriptionData.data.newItem;
        return Object.assign({}, prev, {
          items: {
            items: [newItem, ...prev.items.items],
            count: prev.items.items.length + 1,
            __typename: prev.items.__typename
          }
        });
      }
    });
  };
  _nextPage = data => {
    const page = parseInt(this.props.match.params.page, 10);
    if (
      page < data.items.count / ITEMS_PER_PAGE &&
      data.items.count % ITEMS_PER_PAGE > 0
    ) {
      const nextPage = page + 1;
      this.props.history.push(`/items/${nextPage}`);
    }
  };

  _previousPage = () => {
    const page = parseInt(this.props.match.params.page, 10);
    if (page > 1) {
      const previousPage = page - 1;
      this.props.history.push(`/items/${previousPage}`);
    }
  };
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    items: state.items
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadItems: items => {
      dispatch({ type: "LOAD", payload: items });
    },
    addToCart: item => {
      dispatch({ type: "ADD", payload: item });
    },
    removeFromCart: item => {
      dispatch({ type: "REMOVE", payload: item });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemList);
