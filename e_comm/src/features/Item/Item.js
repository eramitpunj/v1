import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Item extends Component {
  render() {
    const props = this.props;
    return (
      <div className="col-4">
        <div className="card mb-4">
          <img
            src={props.item.url}
            className="card-img-top"
            alt={props.item.name}
          />
          <div className="card-body">
            <div className="row">
              <div className="col">
                <h6 className="card-title">{props.item.name}</h6>
              </div>
              <div className="col text-right text-danger">
                $ {props.item.price.toFixed(2)}
              </div>
            </div>
            <p className="text-muted"> {props.item.description}</p>
            <div className="btn-group">
              <button
                type="button"
                className="btn btn-outline-primary btn-sm"
                onClick={() => props.addToCart(props.item)}
              >
                Add to Cart
              </button>
              <Link
                to={"/item/" + props.item.id}
                className="btn btn-outline-secondary btn-sm"
              >
                More Details
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Item;
