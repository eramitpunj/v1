import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Query } from "react-apollo";
import gql from "graphql-tag";

const DEPTS_QUERY = gql`
  query DeptsQuery {
    departments {
      id
      name
      description
    }
  }
`;

const NEW_DEPTS_SUBSCRIPTION = gql`
  subscription {
    newDepartment {
      id
      name
      description
    }
  }
`;

export class DepartmentList extends Component {
  render() {
    return (
      <Query query={DEPTS_QUERY}>
        {({ loading, error, data, subscribeToMore }) => {
          if (loading)
            return (
              <div className="alert alert-warning">
                Fetching Department List...
              </div>
            );
          if (loading)
            return (
              <div className="alert alert-danger">
                Error Retrieving Department List!!
              </div>
            );

          this._subscribeToNewDepts(subscribeToMore);
          const departments = data.departments;

          return departments.length > 0 ? (
            <ul className="list-group">
              {departments.map(dept => (
                <li
                  key={dept.id}
                  className="list-group-item list-group-item-action"
                >
                  <Link className="alert-link" to={"/dept/" + dept.id}>
                    {dept.name}
                  </Link>
                </li>
              ))}
            </ul>
          ) : (
            <div className="alert alert-danger"> No Department Defined!!</div>
          );
        }}
      </Query>
    );
  }

  _subscribeToNewDepts = subscribeToMore => {
    subscribeToMore({
      document: NEW_DEPTS_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newDepartment = subscriptionData.data.newDepartment;
        return Object.assign({}, prev, {
          departments: [newDepartment, ...prev.departments],
          __typename: prev.departments.__typename
        });
      }
    });
  };
}

export default DepartmentList;
