import React, { Component } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
const ITEM_ATTR_DATA = {
  attrs: [
    { attrName: "attr1", attrValue: "Value 1" },
    { attrName: "attr2", attrValue: "Value 2" },
    { attrName: "attr3", attrValue: "Value 3" },
    { attrName: "attr4", attrValue: "Value 4" }
  ]
};

const ITEM_QUERY = gql`
  query ItemLookup($itemId: ID!) {
    item(itemId: $itemId) {
      id
      name
      description
      url
      price
      createdBy {
        id
        name
      }
    }
  }
`;

export class ItemDetails extends Component {
  render() {
    return (
      <Query
        query={ITEM_QUERY}
        variables={{ itemId: this.props.match.params.itemId }}
      >
        {({ loading, error, data, subscribeToMore }) => {
          if (loading)
            return (
              <div className="alert alert-warning">
                Fetching Item Details...
              </div>
            );
          if (error)
            return (
              <div className="alert alert-danger">
                Error Retrieving Item Details!!
              </div>
            );
          const item = data.item;
          return (
            <div className="col-12">
              <div className="row">
                <div className="col-4">
                  <img src={item.url} alt={item.name} />
                </div>
                <div className="col">
                  <h4>{item.name}</h4>
                  <hr />
                  <div className="row">
                    <div className="col-4 text-danger">
                      <b>
                        {"Price -  $ "}
                        {item.price.toFixed(2)}
                      </b>
                    </div>
                    <div className="col">
                      <button type="button" className="btn btn-sm btn-primary">
                        Add to Cart
                      </button>
                    </div>
                  </div>

                  <p className="text-muted">{item.description}</p>
                  <hr />
                  <h6>Item Attributes</h6>
                  {ITEM_ATTR_DATA.attrs.map((attr, index) => (
                    <div className="row" key={index}>
                      <div className="col-3">
                        <b>{attr.attrName + "   -"}</b>
                      </div>
                      <div className="col text-left">{attr.attrValue}</div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default ItemDetails;
