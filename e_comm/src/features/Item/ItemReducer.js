const initialState = {
  items: []
};
const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOAD":
      return action.payload;
    default:
      return state;
  }
};
export default itemReducer;
