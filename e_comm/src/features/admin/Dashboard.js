import React, { Component } from "react";

export class Dashboard extends Component {
  render() {
    return <div>All the dashboard related charts will be displayed here</div>;
  }
}

export default Dashboard;
