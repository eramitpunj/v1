import React, { Component } from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const ITEM_MUTATION = gql`
  mutation CreateItem(
    $name: String!
    $description: String!
    $price: String!
    $url: String!
  ) {
    createItem(
      name: $name
      description: $description
      price: $price
      url: $url
    ) {
      id
    }
  }
`;

export class CreateItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      description: "",
      price: 0.0,
      url: "/static/images/default_200.png"
    };
  }
  render() {
    const { id, name, description, url, price } = this.state;
    return (
      <div className="row">
        <div className="col">
          <h5>Create an Item</h5>
          <hr />
          {id !== "" ? (
            <div className="alert alert-success">
              The item was successfully created!!
              <br />
              <button
                type="button"
                className="btn btn-sm btn-info"
                onClick={() =>
                  this.setState({
                    id: "",
                    name: "",
                    description: "",
                    price: 0
                  })
                }
              >
                Create Another Item
              </button>
            </div>
          ) : (
            <form>
              <div className="form-group">
                <label>Name</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter your Item Name"
                  onChange={e => this.setState({ name: e.target.value })}
                />
              </div>
              <div className="form-group">
                <label>Price</label>
                <textarea
                  row="3"
                  className="form-control"
                  placeholder="Enter your Item Description"
                  onChange={e => this.setState({ description: e.target.value })}
                />
              </div>
              <div className="form-group">
                <label>Price</label>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"> $ </span>
                  </div>
                  <input
                    type="number"
                    step="0.01"
                    className="form-control"
                    placeholder="Enter your Price"
                    onChange={e => this.setState({ price: e.target.value })}
                  />
                </div>
              </div>
              <Mutation
                mutation={ITEM_MUTATION}
                variables={{ name, description, price, url }}
                onCompleted={data => this._confirmCreation(data)}
              >
                {mutation => (
                  <button
                    type="button"
                    id="btnSave"
                    className="btn btn-success btn-sm"
                    onClick={mutation}
                  >
                    Save
                  </button>
                )}
              </Mutation>
            </form>
          )}
        </div>
      </div>
    );
  }
  _confirmCreation = async data => {
    const itemId = data.createItem.id;
    this.setState({ id: itemId });
  };
}

export default CreateItem;
