import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Menu extends Component {
  render() {
    return (
      <ul className="list-group">
        <li className="list-group-item list-group-item-action">
          <Link className="alert-link" to="/admin/createDepartment">
            Create Department
          </Link>
        </li>
        <li className="list-group-item list-group-item-action">
          <Link className="alert-link" to="/admin/createItem">
            Create Item
          </Link>
        </li>
      </ul>
    );
  }
}

export default Menu;
