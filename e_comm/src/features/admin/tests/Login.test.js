import React from "react";
import { shallow, mount } from "../../../setup/configureEnzyme";
import Login from "../Login";
import { MockedProvider } from "react-apollo/test-utils";

describe("Login component Test Suite", () => {
  it("The Email  input control should be rendered", () => {
    expect(shallow(<Login />).find("#email").length).toEqual(1);
  });

  it("The password  input control should be rendered", () => {
    expect(shallow(<Login />).find("#password").length).toEqual(1);
  });

  it("The Login button should render without any issues", () => {
    const btn = mount(
      <MockedProvider mocks={[]}>
        <Login />
      </MockedProvider>
    );
    expect(btn.find(".btn-success").length).toEqual(1);
    expect(btn.find(".btn-success").get(0).props.children).toEqual("Login");
  });

  it("The Signup button should be rendered", () => {
    expect(shallow(<Login />).find("#btnSignup").length).toEqual(1);
  });
});
