import React, { Component } from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const DEPARTMENT_MUTATION = gql`
  mutation CreateDepartment($name: String!, $description: String!) {
    createDepartment(name: $name, description: $description) {
      id
    }
  }
`;

export class CreateDepartment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      description: ""
    };
  }
  render() {
    const { id, name, description } = this.state;
    return (
      <div className="row">
        <div className="col">
          <h5>Create a Department</h5>
          <hr />
          {id !== "" ? (
            <div className="alert alert-success">
              The department was successfully created!!
              <br />
              <button
                type="button"
                className="btn btn-sm btn-info"
                onClick={() =>
                  this.setState({
                    id: "",
                    name: "",
                    description: ""
                  })
                }
              >
                Create Another Department
              </button>
            </div>
          ) : (
            <form>
              <div className="form-group">
                <label>Name</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter your Item Name"
                  onChange={e => this.setState({ name: e.target.value })}
                />
              </div>
              <div className="form-group">
                <label>Description</label>
                <textarea
                  row="3"
                  className="form-control"
                  placeholder="Enter your Item Description"
                  onChange={e => this.setState({ description: e.target.value })}
                />
              </div>

              <Mutation
                mutation={DEPARTMENT_MUTATION}
                variables={{ name, description }}
                onCompleted={data => this._confirmCreation(data)}
              >
                {mutation => (
                  <button
                    type="button"
                    id="btnSave"
                    className="btn btn-success btn-sm"
                    onClick={mutation}
                  >
                    Save
                  </button>
                )}
              </Mutation>
            </form>
          )}
        </div>
      </div>
    );
  }
  _confirmCreation = async data => {
    const deptId = data.createDepartment.id;
    this.setState({ id: deptId });
  };
}

export default CreateDepartment;
