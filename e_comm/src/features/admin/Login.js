import React, { Component } from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { AUTH_TOKEN } from "./../../common/constants";

const LOGIN_MUTATION = gql`
  mutation LoginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        id
        name
      }
    }
  }
`;

const SIGNUP_MUTATION = gql`
  mutation SignupMutation($email: String!, $password: String!, $name: String!) {
    signup(name: $name, email: $email, password: $password) {
      token
      user {
        id
        name
      }
    }
  }
`;

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: true,
      name: "",
      email: "",
      password: ""
    };
  }
  render() {
    const { login, name, email, password } = this.state;
    return (
      <div className="row justify-content-center">
        <div className="col-6 alert alert-secondary">
          {!login && (
            <div className="form-group">
              <label>Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                placeholder="Enter your Name"
                value={name}
                onChange={e => this.setState({ name: e.target.value })}
              />
            </div>
          )}
          <div className="form-group">
            <label>Email Address</label>
            <input
              type="email"
              className="form-control"
              id="email"
              placeholder="Enter your Email"
              value={email}
              onChange={e => this.setState({ email: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="text"
              className="form-control"
              id="password"
              placeholder="Enter your Password"
              value={password}
              onChange={e => this.setState({ password: e.target.value })}
            />
          </div>
          <Mutation
            mutation={login ? LOGIN_MUTATION : SIGNUP_MUTATION}
            variables={{ email, password, name }}
            onCompleted={data => this._confirmLogin(data)}
          >
            {mutation => (
              <button
                type="submit"
                id="btnLogin"
                className="btn btn-success btn-sm"
                onClick={mutation}
              >
                {login ? "Login" : "Create An Account"}
              </button>
            )}
          </Mutation>
          {"   "}
          <button
            type="button"
            id="btnSignup"
            className="btn btn-secondary btn-sm"
            onClick={() =>
              this.setState({
                login: !login
              })
            }
          >
            {login ? "Signup" : "Already have an Account"}
          </button>
        </div>
      </div>
    );
  }
  _confirmLogin = async data => {
    const { token } = this.state.login ? data.login : data.signup;
    this._saveUserData(token);
    this.props.history.push("/");
  };

  _saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token);
  };
}

export default Login;
