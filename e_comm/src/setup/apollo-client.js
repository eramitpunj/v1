import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { WebSocketLink } from "apollo-link-ws";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { getMainDefinition } from "apollo-utilities";
import { split } from "apollo-link";

import { AUTH_TOKEN } from "./../common/constants";

// Link for http requests, usually used with mutation and queries
const httpLink = createHttpLink({ uri: "http://localhost:4000" });

/**
 * The http Link needs to have authentication details
 * this is to append the authentication details in
 * request header
 */
const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(AUTH_TOKEN);
  return {
    headers: {
      ...headers,
      authorization: token ? `ScapeGoat ${token}` : ""
    }
  };
});

// Link for web sockets, used for subscriptions
// The authentication details are being read from local storage
// this is not recommended but being used for this challenge
const wsLink = new WebSocketLink({
  uri: "ws://localhost:4000",
  options: {
    reconnect: true,
    connectionParams: {
      authToken: localStorage.getItem(AUTH_TOKEN)
    }
  }
});

// The split helps in identifying if the websocket or http link should be
// used for the GraphQL calls based on operation name and type
const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === "OperationDefinition" && operation === "subscription";
  },
  wsLink,
  authLink.concat(httpLink)
);

//Create the client for Apollo which will be added to the application
const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

export default client;
