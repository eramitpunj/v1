import { createStore } from "redux";
import rootReducer from "../features/rootReducer";
/**
 * This file is to create the store from redux reducers
 *
 */

const store = createStore(rootReducer);
export default store;
