const jwt = require("jsonwebtoken");
const { APP_SECRET } = require("./constants");

function getUserId(context) {
  const authHeader = context.request.get("Authorization");
  if (authHeader) {
    const token = authHeader.replace("ScapeGoat ", "");
    const { userId } = jwt.verify(token, APP_SECRET);
    return userId;
  }
  throw new Error("Not authenticated");
}

module.exports = {
  getUserId
};
