/**
 * This is to created a subscription server for new item
 */
const newItem = {
  subscribe: newItemSubscribe,
  resolve: payload => {
    return payload;
  }
};

//Returns the item node after the CREATED event
function newItemSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe.item({ mutation_in: ["CREATED"] }).node();
}

/**
 * This is to created a subscription server for new department
 */
const newDepartment = {
  subscribe: newDepartmentSubscribe,
  resolve: payload => {
    return payload;
  }
};
//Returns the department node after the CREATED event
function newDepartmentSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .department({ mutation_in: ["CREATED"] })
    .node();
}

module.exports = {
  newItem,
  newDepartment
};
