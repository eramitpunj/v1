/**
 * This is to pull the item details created by user
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
function items(parent, args, context) {
  return context.prisma.user({ id: parent.id }).items();
}

/**
 * This is to pull the department details created by user
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
// function departments(parent, args, context, info) {
//   return context.prisma.user({ id: parent.id }).departments();
// }

module.exports = {
  items
};
