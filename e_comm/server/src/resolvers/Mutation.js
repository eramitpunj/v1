const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { APP_SECRET } = require("../common/constants");
const { getUserId } = require("../common/utils");

/**
 * This resolver is used to persist the new user details in database
 * Also, the secret key is used to create a token which will be used
 * to authenticate users for other persist operations
 *
 * @param {*} parent
 * @param {*} args - contains all the values sent from client
 * @param {*} context - contains the objects for prisma client
 * @param {*} info
 */
async function signup(parent, args, context, info) {
  const password = await bcrypt.hash(args.password, 10);
  const user = await context.prisma.createUser({
    ...args,
    password
  });

  const token = jwt.sign({ userId: user.id }, APP_SECRET);

  return {
    token,
    user
  };
}

/**
 * This resolver is used to validate if the user is a valid user
 * and if the provided password matches the password in database
 * if everything is a success, a token is generated and sent back
 *
 * @param {*} parent
 * @param {*} args - contains all the values sent from client
 * @param {*} context - contains the objects for prisma client
 * @param {*} info
 */
async function login(parent, args, context, info) {
  const user = await context.prisma.user({ email: args.email });
  if (!user) {
    throw new Error("User does not exists!!");
  }

  const valid = await bcrypt.compare(args.password, user.password);
  if (!valid) {
    throw new Error("Invalid Password!!");
  }
  return {
    token: jwt.sign({ userId: user.id }, APP_SECRET),
    user
  };
}

/**
 * This resolver is to save the item information in the database
 *
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
async function createItem(parent, args, context, info) {
  //Validate if the user is valid based on header information
  const userId = getUserId(context);

  //Save the item details with user and price
  return context.prisma.createItem({
    name: args.name,
    description: args.description,
    url: args.url,
    price: parseFloat(args.price),
    createdOn: new Date(),
    createdBy: { connect: { id: userId } }
  });
}

/**
 * This resolver is to save the department information in the database
 *
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
async function createDepartment(parent, args, context, info) {
  //Validate if the user is valid based on header information
  const userId = getUserId(context);
  //Save the department information
  return context.prisma.createDepartment({
    name: args.name,
    description: args.description
  });
}

module.exports = {
  signup,
  login,
  createItem,
  createDepartment
};
