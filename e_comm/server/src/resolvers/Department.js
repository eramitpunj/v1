/**
 * This is to pull the user details for an department type
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
function createdBy(parent, args, context, info) {
  return context.prisma.department({ id: parent.id }).createdBy();
}

module.exports = {
  createdBy
};
