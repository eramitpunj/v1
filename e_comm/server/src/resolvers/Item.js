/**
 * This is to pull the user details for an item type
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
function createdBy(parent, args, context) {
  return context.prisma.item({ id: parent.id }).createdBy();
}

module.exports = {
  createdBy
};
