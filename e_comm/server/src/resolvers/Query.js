//This function looks up the department based on provided id
async function department(parent, args, context, info) {
  return await context.prisma.department({ id: args.departmentId });
}

//This function looks up all the departments
async function departments(parent, args, context, info) {
  return await context.prisma.departments();
}

//This function looks up the item based on provided id
async function item(parent, args, context, info) {
  return await context.prisma.item({ id: args.itemId });
}

/**
 * This function retrieves the items from database based on the provided criteria.
 * The prisma client object is retrieved from context and the client side data is
 * available in args object for the operation. The pagination is implemented using
 *  skip, first properties in case of huge results
 *
 * @param {*} parent
 * @param {*} args
 * @param {*} context
 * @param {*} info
 */
async function items(parent, args, context, info) {
  const count = await context.prisma
    .itemsConnection({
      where: {
        OR: [
          { name_contains: args.filter },
          { description_contains: args.filter }
        ]
      }
    })
    .aggregate()
    .count();

  const items = await context.prisma.items({
    where: {
      OR: [
        { name_contains: args.filter },
        { description_contains: args.filter }
      ]
    },
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy
  });

  return {
    count,
    items
  };
}

module.exports = {
  department,
  departments,
  item,
  items
};
