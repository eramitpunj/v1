/**
 * This file is to create the server application GraphQL server
 * using graphqlyoga api
 * Also, this will add the prisma object to access database in context
 */
const { GraphQLServer } = require("graphql-yoga");
const { prisma } = require("./generated/prisma-client");
const Query = require("./resolvers/Query");
const Mutation = require("./resolvers/Mutation");
const Subscription = require("./resolvers/Subscription");
const User = require("./resolvers/User");
//const Department = require("./resolvers/Department");
const Item = require("./resolvers/Item");

//The resolvers will be defined for the APIs in different files
const resolvers = { Query, Mutation, Subscription, User, Item };

// The GraphQL server needs all the types and resolvers for the GraphQL operations
// The types are defined in a seperate file called schema.graphql
// The resolvers are defined in seperate files as imported
const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: request => ({
    ...request,
    prisma
  })
});

server.start(() =>
  console.log("E-Commerce Server is running on http://localhost:4000")
);
