#V1 Coding Challenge (E-Commerce App)

The E-Commerce website needs to have:

- An Administrator interface that should allow changes that would be reflected within
  the customer interface.
- The changes (mutations) are sent to a GraphQL endpoint.
- Customer interface to query a GraphQL endpoint.

###Requirements:
Make conscious choices to optimize getting something minimal but solid up and
running quickly.

- We expect to be able to easily install your project and get it up and running locally to
  check out your work.
- The UI and UX of the project are up to you, but don't spend time worrying about it.
  We just care about your code :)
- Your solution should include at least one good test.

###Completed -
There is an admin menu which will appear when the user successfully logins. This menu will open a page which has the following 2 functionalities :

1. Create Item - This page will help creating a new item which will be saved in backend. The newly created item will be reflected on item listing page on home screen of the application automatically.
2. Create Department - This page will help creating a new department which will be saved in backend. The newly department item will be reflected on left side department area on home screen of the application automatically.
   #### Test Case:
   I have included the basic unit test case for Login screen in client application.

####In Progress/ Planned/ Issues -
#####\* Issues:

1. The prisma server part was giving some issue and I could not resolve it within given time.
2. The test case for Login component was not fixed after the mutation code was written. The login button won't appear with "shallow" but it will work with "mount". Also, the test case should include Apollo mock provider as well.

#####\* Planned:

1. Cart functionality
2. Check out functionality

## Running the Application

There are 2 parts to this application which are explained as below:
##1. Server -
This application deals with handling the GraphQL server side logic to server queries, mutations and subscriptions as defined in GraphQL schema. Also, this is responsible for saving it to a database on prisma server.

The following setup is need for the server to run on your machine:

Step 1. Go to your project root and 'server' folder.
Step 2. Execute the following command to install prisma at global level. This is used to run commands related to prisma database setup.
`npm install -g prisma`

Step 3. Register with Prisma Cloud at [https://www.prisma.io/cloud](https://www.prisma.io/cloud) and go back to your terminal.
Execute the following command to deploy data model defined in application to prisma cloud.
Also, this command will generate the prisma client as well automatically which is being used in application to connect with the database.

    `prisma deploy`

- First select the Demo server. When the browser opens, provide details as registered earlier and go back to your terminal.
- Then you need to select the region for your Demo server. Once that’s done, you can just hit enter twice to use the suggested values for service and stage.
- Once the command has finished running, the CLI writes the endpoint for the Prisma API to your prisma.yml. It will look similar to this: https://eu1.prisma.sh/amit-punj/prisma/dev

Step 4. Once the step 3 is completed, execute the following commands to execute the server application at [http://localhost:4000](http://localhost:4000)

    `npm install`
    `npm start`

##2. Client -
This application is the frontend of E-Commerce application. This application is based on react, redux and uses GraphQL to connect with server and database contents.
Go to project root directory and you can run:

    `npm install`
    `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

    `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

    `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
