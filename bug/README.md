# Bug Test V1.1

This is a bug test where we evaluate your ability to resolve bugs based on console log messages you get. We also want you to demonstrate in the code (with comments) what was the bug, how you resolved it and as you inspect the code if there are ways the code could be improved.

## In summary in this test, do the following:

- Review each bug and fix them
- Comment what was the bug and how you resolved it
- Inspect the code to find improvements in style, best practices and logic
- Make the improvements with comments as to why you'd make those improvements

## Bug Fixes, Findings and Comments

| Fix           | Modified File               | Details                                                                                                                                                       |
| ------------- | --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Fix 1         | src/components/Footer.js    | There was an typo extends was written as extend                                                                                                               |
| Fix 2         | src/containers/App.js       | The LoginForm was not imported in App.js                                                                                                                      |
| Fix 3         | src/index.js                | The bootstrap dependency has not been imported in the project along with react-bootstrap as per react-bootstrap dependency installing bootstrap version 3.3.7 |
| Fix 4         | src/containers/App.js       | The naming convention for component navbar.js is changed from navbar to Navbar to match with other components                                                 |
| Fix 5         | src/components/Navbar.js    | The state keyword is missing in this line 11                                                                                                                  |
| Fix 6         | src/components/LoginForm.js | The props keyword was missing as handleLogin is a prop                                                                                                        |
| Fix 7         | src/containers/App.js       | Deleted the unnecessary code in handleLogin                                                                                                                   |
| Fix 8         | src/containers/App.js       | bind the handleLogin function to this so that we can access state                                                                                             |
| Fix 9         | src/containers/App.js       | The login form should hide when the login is successful                                                                                                       |
| Fix 10        | src/components/Navbar.js    | The logout button should be visible based on provided property, there is no need for local state variable in that                                             |
| Fix 11        | src/containers/App.js       | Deleted the unnecessary code in handleLogout                                                                                                                  |
| Improvement 1 | src/containers/App.js       | The state should be as small as possible                                                                                                                      |
| Improvement 2 | src/containers/App.js       | Smaller syntax for boolean check and component rendering                                                                                                      |
| Improvement 3 | src/containers/App.js       | Reduce the size of state as we can drive showCheckmark based on LoginForm state                                                                               |
| Improvement 4 | src/components/Navbar.js    | Removed unnecessary style imports                                                                                                                             |
| Improvement 4 | src/components/Footer.js    | Removed unnecessary style imports                                                                                                                             |
| Improvement 4 | src/components/LoginForm.js | Removed unnecessary style imports                                                                                                                             |

## Starting the application

Download the application on you machine which has node already install.
Execute the following commands

```
node install
node start
```

## Good luck!
