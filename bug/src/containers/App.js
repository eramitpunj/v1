import React, { Component } from "react";
// Fix 4 - Navbar file name was changes as per standards and to synch with code
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
// Fix 2 - LoginForm was missing in imports
import LoginForm from "../components/LoginForm";
import { Glyphicon } from "react-bootstrap";
import "../styles/App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoginForm: true,
      showCheckmark: false
    };
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogin() {
    // Fix 7 - Deleted the unnecessary code
    // this.refs.navbutton.handleLogoutButton();
    // Fix 8 - bing the handleLogin function to this so that we can access state
    // Fix 9 - The login form should hide when the login is successful
    // Improvement 3 - Reduce the size of state as we can drive showCheckmark based on LoginForm state
    this.setState({
      showLoginForm: false
      // showCheckmark: true
    });
  }

  handleLogout() {
    // Fix 11 - Deleted the unnecessary code
    this.setState({
      showLoginForm: true
      // showCheckmark: false
    });
  }

  render() {
    return (
      <div className="app">
        {/* 
          Fix 10 - The logout button should be visible when login is success
          Adding a new property to handle the scenario
          We can use state.showLoginForm as only either of these components
          can be visible at the same time
        */}
        <Navbar
          showLogoutButton={!this.state.showLoginForm}
          handleLogout={this.handleLogout}
        />
        {/* 
          Improvement 2 - Smaller syntax for the coding
        */}
        {this.state.showLoginForm && (
          <LoginForm handleLogin={this.handleLogin} />
        )}
        <div className={this.state.showLoginForm ? "hide" : "text-center mt9x"}>
          <Glyphicon glyph="glyphicon glyphicon-ok-sign" />
          <h2>Great work!</h2>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
