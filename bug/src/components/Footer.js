import React, { Component } from "react";
// Improvement 4 - removed the unnecessary style import
// import "../styles/App.css";

// Fix 1 - There was a typo in 'extend' it should be extends instead of extend
class Footer extends Component {
  render() {
    return (
      <div>
        <div className="footer-anchor" />
        <div className="app-footer" />
      </div>
    );
  }
}

export default Footer;
