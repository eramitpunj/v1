import React, { Component } from "react";
import { Col, Form, FormGroup, FormControl, Glyphicon } from "react-bootstrap";
// Improvement 4 - removed the unnecessary style import
// import "../styles/App.css";

class LoginForm extends Component {
  onFormSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      // Improvement - The form submit type should be post for security reason
      <Form
        horizontal
        type="post"
        className="mt9x"
        onSubmit={this.props.handleLogin}
      >
        <fieldset>
          <Col smOffset={4} sm={6}>
            <FormGroup>
              <Col sm={8}>
                <h3 className="text-left text-gray">Sign in</h3>
              </Col>
            </FormGroup>

            {/* 
              Improvement 1 - Make the email and password mandatory
              */}
            <FormGroup controlId="formHorizontalEmail">
              <Col sm={8}>
                <FormControl
                  className="input-line"
                  type="email"
                  name="email"
                  placeholder="Email Address"
                  autoCapitalize="off"
                  autoCorrect="off"
                  required
                />
              </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalPassword">
              <Col sm={8}>
                <FormControl
                  className="input-line"
                  type="password"
                  name="password"
                  placeholder="Password"
                  required
                />
              </Col>
            </FormGroup>
            {/* 
              Fix 6 - The props keyword was missing as handleLogin is a prop
              */}
            <FormGroup>
              <Col sm={8} className="mt6x">
                <button className="flat-button border-gray" type="submit">
                  Next
                  <Glyphicon className="pl2x" glyph="menu-right" />
                </button>
              </Col>
            </FormGroup>
          </Col>
        </fieldset>
      </Form>
    );
  }
}

export default LoginForm;
