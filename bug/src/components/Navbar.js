import React, { Component } from "react";
// Improvement 4 - removed the unnecessary style import
// import "../styles/App.css";

class Navbar extends Component {
  /*  
      Fix 10 - The logout button should be visible based on provided property 
      There is no need for local state.
      I am commenting the code for review, it will be deleted in production version
    */
  // Fix 5 - The state keyword was missing to initialize the state
  // constructor(props) {
  //   super(props);

  //   this.state = { showLogoutButton: false };
  // }

  // handleLogoutButton() {
  //   this.setState(prevState => ({
  //     showLogoutButton: !prevState.showLogoutButton
  //   }));
  // }

  render() {
    let sessionButton;
    /*  
      Fix 10 - The logout button should be visible based on provided property 
      There is no need for local state variable in that
    */

    if (this.props.showLogoutButton) {
      sessionButton = (
        <button
          className="flat-button border-gray"
          onClick={this.props.handleLogout}
        >
          Sign Out
        </button>
      );
    }
    return (
      <div className="app-navbar">
        <div className="flex-container">
          <div className="header">React Debug App</div>
          {sessionButton}
        </div>
      </div>
    );
  }
}

export default Navbar;
