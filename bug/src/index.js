import React from "react";
import ReactDOM from "react-dom";
import App from "./containers/App";
// Fix 3 - The bootstrap css dependency for react-bootstrap has been installed, it was missing
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import "./styles/App.css";

ReactDOM.render(<App />, document.getElementById("root"));
